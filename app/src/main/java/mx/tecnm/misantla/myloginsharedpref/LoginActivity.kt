package mx.tecnm.misantla.myloginsharedpref

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    var preferencias :SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        preferencias = PreferenceManager.getDefaultSharedPreferences(this)

        ponerPreferenciasExisten()

        btnlogin.setOnClickListener {
            val email = Edt_Email.text.toString()
            val pass = Edt_password.text.toString()

            if(logeo(email, pass)){
                val intent =  Intent(this,MainActivity :: class.java)
                guardarPreferencias(email,pass)
                startActivity(intent)

            }
        }
    }

    fun validadEmail(email:String):Boolean{
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun validarPass(pass:String): Boolean{
        return pass.length >=8
    }

    fun logeo(email: String, pass: String):Boolean{
        if(!validadEmail(email)){
            //toast Email no valido
            Toast.makeText(this, "Email Invalido",Toast.LENGTH_SHORT).show()
            return false
        }else if(!validarPass(pass)){
            // toast Password no valido
            Toast.makeText(this,"Password debe ser > 7 caracteres",Toast.LENGTH_SHORT).show()
            return false
        }else{
            return true
        }

    }

    fun guardarPreferencias(email: String,pass: String){
      if(switch_Recordar.isChecked){
          preferencias!!.edit()
              .putString("email",email)
              .putString("pass",pass)
              .apply()

      }
    }
    fun  ponerPreferenciasExisten(){
        val email = preferencias!!.getString("email","")
        val pass = preferencias!!.getString("pass","")
        if(!email.isNullOrEmpty() && pass.isNullOrEmpty()){
            Edt_Email.setText(email)
            Edt_password.setText(pass)
            switch_Recordar.isChecked = true
        }
    }
}